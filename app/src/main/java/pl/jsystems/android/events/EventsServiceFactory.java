package pl.jsystems.android.events;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EventsServiceFactory {
    private static EventsService service;

    public static EventsService getService() {
        if (service == null) {
            service = createEventsService();
        }
        return service;
    }

    private static EventsService createEventsService() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("http://urbanpawel90.usermd.net/")
                .build();
        return retrofit.create(EventsService.class);
    }
}
