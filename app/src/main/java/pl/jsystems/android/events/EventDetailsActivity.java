package pl.jsystems.android.events;

import java.text.DateFormat;
import java.util.List;
import java.util.UUID;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventDetailsActivity extends AppCompatActivity {
    private TextView tvName, tvLocation, tvDate, tvPrice, tvWww;
    private ImageButton btnCommentAdd;
    private RecyclerView rvComments;

    private UUID eventId;

    @Override
    protected void onResume() {
        super.onResume();
        loadEvent();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        tvName = findViewById(R.id.tv_name);
        tvLocation = findViewById(R.id.tv_location);
        tvDate = findViewById(R.id.tv_date);
        tvPrice = findViewById(R.id.tv_price);
        tvWww = findViewById(R.id.tv_www);
        btnCommentAdd = findViewById(R.id.btn_comment_add);
        rvComments = findViewById(R.id.rv_comments);

        btnCommentAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCommentAddClick();
            }
        });

        readEventId();
    }

    private void loadEvent() {
        EventsServiceFactory.getService().getSingleEvent(eventId.toString()).enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                Event event = response.body();
                displayComments(event.getCommentList());

                tvName.setText(event.getName());
                tvLocation.setText(event.getLocation());
                tvPrice.setText(event.getPrice().toString());
                tvWww.setText(event.getWwwUrl());
                tvDate.setText(DateFormat.getDateTimeInstance().format(event.getDate()));
            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void displayComments(List<Comment> comments) {
        CommentsAdapter adapter = new CommentsAdapter();
        adapter.setComments(comments);
        rvComments.setAdapter(adapter);
    }

    private void readEventId() {
        if (getIntent().hasExtra("id")) {
            String strEventId = getIntent().getStringExtra("id");
            eventId = UUID.fromString(strEventId);
        } else {
            finish();
        }
    }

    private void onCommentAddClick() {
        Intent addCommentIntent =
                new Intent(this, AddCommentActivity.class);
        addCommentIntent.putExtra("id", eventId.toString());
        startActivity(addCommentIntent);
    }
}
