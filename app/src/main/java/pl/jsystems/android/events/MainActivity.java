package pl.jsystems.android.events;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements EventsAdapter.EventClickListener {
    private RecyclerView rvEvents;
    private View vOverlay;
    private ProgressBar pbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvEvents = findViewById(R.id.rv_events);
        vOverlay = findViewById(R.id.v_overlay);
        pbLoading = findViewById(R.id.pb_loading);

        loadEvents();
    }

    private void loadEvents() {
        EventsServiceFactory.getService().getEvents().enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, final Response<List<Event>> response) {
                // Handler podpina sie pod kolejke zadan glownego watku i planuje
                // wykonanie zadanie wyświetlenia listy z 2sek opóźnieniem (2000ms).
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        displayEvents(response.body());
                        hideLoading();
                    }
                }, 2000);
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
            }
        });
    }

    private void displayEvents(List<Event> events) {
        EventsAdapter adapter = new EventsAdapter(this);
        adapter.setEvents(events);
        rvEvents.setAdapter(adapter);
    }

    private void hideLoading() {
        vOverlay.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
    }

    @Override
    public void onEventClick(Event event) {
        Intent detailsIntent = new Intent(this, EventDetailsActivity.class);
        detailsIntent.putExtra("id", event.getId().toString());
        startActivity(detailsIntent);
    }
}
