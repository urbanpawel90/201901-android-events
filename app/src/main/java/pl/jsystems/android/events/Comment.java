package pl.jsystems.android.events;

import java.util.Date;
import java.util.UUID;

public class Comment {
    private UUID id;
    private Date createDate;
    private String content;
    private String authorNick;
    private String authorEmail;
    private String authorWWW;
    private CommentRank rate;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthorNick() {
        return authorNick;
    }

    public void setAuthorNick(String authorNick) {
        this.authorNick = authorNick;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getAuthorWWW() {
        return authorWWW;
    }

    public void setAuthorWWW(String authorWWW) {
        this.authorWWW = authorWWW;
    }

    public CommentRank getRate() {
        return rate;
    }

    public void setRate(CommentRank rate) {
        this.rate = rate;
    }
}
