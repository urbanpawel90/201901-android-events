package pl.jsystems.android.events;

import java.text.DateFormat;
import java.util.List;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHolder> {
    private List<Comment> comments;

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View rowView = inflater.inflate(R.layout.row_comment, viewGroup, false);
        return new CommentViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder commentViewHolder, int i) {
        Comment comment = comments.get(i);

        commentViewHolder.tvComment.setText(comment.getContent());
        commentViewHolder.tvAuthor.setText(String.format("%s <%s>\n%s",
                comment.getAuthorNick(),
                comment.getAuthorEmail(),
                comment.getAuthorWWW()));
        commentViewHolder.tvDate.setText(
                DateFormat.getDateTimeInstance().format(comment.getCreateDate()));
        commentViewHolder.ivRank.getDrawable().setLevel(
                comment.getRate() == CommentRank.Pozytywny ? 1 : 3);
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        private TextView tvComment, tvAuthor, tvDate;
        private ImageView ivRank;

        public CommentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvComment = itemView.findViewById(R.id.tv_comment);
            tvAuthor = itemView.findViewById(R.id.tv_author);
            tvDate = itemView.findViewById(R.id.tv_date);
            ivRank = itemView.findViewById(R.id.iv_rank);
        }
    }
}
