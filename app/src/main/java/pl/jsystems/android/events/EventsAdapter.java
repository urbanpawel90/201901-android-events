package pl.jsystems.android.events;

import java.text.DateFormat;
import java.util.List;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> {
    private List<Event> events;
    private EventClickListener clickListener;

    public EventsAdapter(EventClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View rowView = inflater.inflate(R.layout.row_event, viewGroup, false);
        return new EventViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder eventViewHolder, int i) {
        Event event = events.get(i);

        eventViewHolder.tvName.setText(event.getName());
        eventViewHolder.tvLocation.setText(event.getLocation());
        // TODO: Format daty zalezny od ustawien
        eventViewHolder.tvDate.setText(DateFormat.getDateTimeInstance()
                .format(event.getDate()));
        eventViewHolder.currentEvent = event;
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvName, tvLocation, tvDate;
        private Event currentEvent;

        public EventViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_name);
            tvLocation = itemView.findViewById(R.id.tv_location);
            tvDate = itemView.findViewById(R.id.tv_date);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onEventClick(currentEvent);
        }
    }

    public interface EventClickListener {
        void onEventClick(Event event);
    }
}
