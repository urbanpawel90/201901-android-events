package pl.jsystems.android.events;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface EventsService {
    @GET("events/event")
    Call<List<Event>> getEvents();

    @GET("events/event/{id}")
    Call<Event> getSingleEvent(@Path("id") String eventId);

    @POST("events/event/{id}/comment")
    Call<Void> createComment(@Path("id") String eventId, @Body Comment comment);
}
