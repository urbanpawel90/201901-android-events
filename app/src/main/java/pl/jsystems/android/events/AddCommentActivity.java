package pl.jsystems.android.events;

import java.util.Date;
import java.util.UUID;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCommentActivity extends AppCompatActivity {
    private EditText etAuthor, etEmail, etWww, etComment;
    private RadioGroup rgRank;
    private Button btnSave;

    private UUID eventId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);

        etAuthor = findViewById(R.id.et_author);
        etEmail = findViewById(R.id.et_email);
        etWww = findViewById(R.id.et_www);
        etComment = findViewById(R.id.et_comment);
        rgRank = findViewById(R.id.rg_rank);
        btnSave = findViewById(R.id.btn_save);

        readEventId();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveCommentClick();
            }
        });
    }

    private void onSaveCommentClick() {
        Comment comment = new Comment();
        comment.setCreateDate(new Date());
        comment.setContent(etComment.getText().toString());
        comment.setAuthorNick(etAuthor.getText().toString());
        comment.setAuthorEmail(etEmail.getText().toString());
        comment.setAuthorWWW(etWww.getText().toString());
        if (rgRank.getCheckedRadioButtonId() == R.id.rb_positive) {
            comment.setRate(CommentRank.Pozytywny);
        } else {
            comment.setRate(CommentRank.Negatywny);
        }

        EventsServiceFactory.getService().createComment(eventId.toString(), comment).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    finish();
                } else {
                    onFailure(call, new RuntimeException("Blad serwera!"));
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void readEventId() {
        if (getIntent().hasExtra("id")) {
            String strEventId = getIntent().getStringExtra("id");
            eventId = UUID.fromString(strEventId);
        } else {
            finish();
        }
    }
}
